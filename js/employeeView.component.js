( function( angular ) {

	angular.module( 'employeeModule' )
		.controller( 'employeeController', function( employeeService ) {
			var employeePromise;
			var vm = this;
			vm.loadStatus = false;

			//vm.loadEmployees = function() {
			//	employeePromise = employeeService.getEmployees();
			//	employeePromise.then( function( data ) {
			//		vm.employeeList = data;
			//	} ).then( function( result ) {
			//		//Result is actually undefined.
			//		console.log( 'result of chain', result );
			//	});
			//
			//	employeePromise.then( function( data ) {
			//		vm.loadStatus = true;
			//	} );
			//
			//};
		})

		.component( 'employeeView', {
			templateUrl: 'js/employeeView.html',
			controller: 'employeeController',
			controllerAs: 'ec',
			bindings: {
				buttonLabel: '@label',
				employeeList: '='
			}
		})

})( window.angular );