(function ( angular ) {
	'use strict';

	angular
		.module( 'filter.module', [] )
		.filter( 'loaded', function() {
			return function( input ) {

				return input ? '\u2713 Logged in' : '\u2718 Not logged in';
			}
		})
})( window.angular );