( function( angular ) {

	angular.module( 'abcModule', [ 'ngRoute', 'employeeModule', 'filter.module' ] )
		.config( function( $routeProvider ) {
			$routeProvider
				.when( '/about', {
					template: '<h2>About Page</h2>' +
					'<a href="#/employees">Go To Employees</a>'
				})
				.when( '/employees', {
					template: '<employee-view label="Load Employees" employee-list="$resolve.employeeList"></employee-view>',
					resolve: {
						'employeeList': function( employeeService ) {
							return employeeService.getEmployees();
						}
					}
				})
				.otherwise({
					redirectTo: '/about'
				})
		} );

})( window.angular );