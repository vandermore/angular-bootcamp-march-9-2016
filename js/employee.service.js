( function( angular ) {

	angular.module( 'employeeModule', [] )
		.service( 'employeeService', function( $http ) {
			this.getEmployees = function() {
				return $http.get( '/demo-data/employees.json' ).then( employeeResults );
			};

			function employeeResults( result ) {
				return result.data;
			}
		})

})( window.angular );